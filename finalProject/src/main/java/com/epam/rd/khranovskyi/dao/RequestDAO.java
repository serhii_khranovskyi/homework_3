package com.epam.rd.khranovskyi.dao;

import com.epam.rd.khranovskyi.dao.entity.Request;
import org.apache.log4j.Logger;

import java.sql.*;

public class RequestDAO {
    private static final Logger LOGGER = Logger.getLogger(RequestDAO.class);

    private static final String SQL__TRANSACTION_ACTIVITY_USERS_SELECT_ID_BY_MAIL =
            "SELECT id_users FROM users WHERE mail = ?";

    private static final String SQL__TRANSACTION_ACTIVITY_USERS_SELECT_ACTIVITY_ID_BY_NAME =
            "select id_activities from activities where activity_name = ?;";

    private static final String SQL__TRANSACTION_ACTIVITY_USERS_INSERT =
            "INSERT INTO activities_users" +
                    " (activities_id_activities, users_id_users, `beginning`, `ending`,status) " +
                    "VALUES (?,?,?,?,?);";

    private static final String SQL__ACTIVITY_USERS_INSERT =
            "INSERT INTO activities_users" +
                    " (activities_id_activities, users_id_users, status) " +
                    "VALUES (?,?,?);";
    private static final String SQL__ACTIVITY_USERS_UPDATE_STATUS =
            "update activities_users SET status = ? " +
                    "where users_id_users = ? AND activities_id_activities = ?;";

    private static final String SQL__TRANSACTION_ACTIVITY_USERS_UPDATE =
            "update activities_users SET beginning = ?, ending = ?, status = ? " +
                    "where id_activities_userscol = ?;";

    private static final String SQL__ACTIVITY_USERS_UPDATE =
            "update activities_users SET beginning = ?, ending = ? " +
                    "where users_id_users = ? AND activities_id_activities = ?;";

    private static final String SQL__TRANSACTION_ACTIVITY_USERS_DELETE =
            "delete from activities_users where id_activities_userscol = ?;";

    private static final String SQL__GET_USERS_ACTIVITY_ID =
            "select id_activities_userscol, beginning, ending from activities_users where users_id_users = ? and activities_id_activities =? and status =2;";

    /**
     * @param mail
     * @param activityName
     * @param status
     * @param start
     * @param end
     */
    public void insert(String mail, String activityName, int status, Timestamp start, Timestamp end, String language) {

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            LOGGER.trace("Get con");

            //Get the user`s id
            PreparedStatement ps = con.prepareStatement(SQL__TRANSACTION_ACTIVITY_USERS_SELECT_ID_BY_MAIL);
            ps.setString(1, mail);
            rs = ps.executeQuery();
            rs.next();
            int userID = rs.getInt("id_users");

            int activityId = 0;
            //get the activities Id, based on language
            if (language.equals("ru")) {
                PreparedStatement preparedStatement = con.prepareStatement(SQL__TRANSACTION_ACTIVITY_USERS_SELECT_ACTIVITY_ID_BY_NAME);
                preparedStatement.setString(1, activityName);
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                activityId = resultSet.getInt("id_activities");
                preparedStatement.close();
                resultSet.close();
            } else {
                PreparedStatement preparedStatement1 = con.prepareStatement("select activities_id_activities from activities_localization where translation =?;");
                preparedStatement1.setString(1, activityName);
                ResultSet resultSet1 = preparedStatement1.executeQuery();
                resultSet1.next();
                activityId = resultSet1.getInt("activities_id_activities");
                preparedStatement1.close();
                resultSet1.close();
            }

            //insert the new record into the activities_users table
            pstmt = con.prepareStatement(SQL__TRANSACTION_ACTIVITY_USERS_INSERT);
            pstmt.setLong(1, activityId);
            pstmt.setLong(2, userID);
            pstmt.setTimestamp(3, start);
            pstmt.setTimestamp(4, end);
            pstmt.setInt(5, status);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            LOGGER.error(ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }

    }

    /**
     * Insert the new record into the activities_users table
     *
     * @param userId
     * @param activityId
     * @param status
     */

    public void insert(Long userId, Long activityId, int status) {

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            LOGGER.trace("Get con");

            pstmt = con.prepareStatement(SQL__ACTIVITY_USERS_INSERT);
            pstmt.setLong(1, activityId);
            pstmt.setLong(2, userId);
            pstmt.setInt(3, status);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            LOGGER.error("Error after inserting into activities_usres table --> " + ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * Updating the activity beginning
     *
     * @param requestId
     * @param timestamp
     */
    public void updateStart(long requestId, Timestamp timestamp) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            LOGGER.trace("Get con");
            pstmt = con.prepareStatement("update activities_users set beginning =? where id_activities_userscol = ?;");
            pstmt.setTimestamp(1, timestamp);
            pstmt.setLong(2, requestId);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            LOGGER.error(ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * update the activity ending
     *
     * @param requestId
     * @param timestamp
     */
    public void updateEnd(long requestId, Timestamp timestamp) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            LOGGER.trace("Get con");


            pstmt = con.prepareStatement("update activities_users set ending =?, status = 5 where id_activities_userscol = ?;");
            pstmt.setTimestamp(1, timestamp);
            pstmt.setLong(2, requestId);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            LOGGER.error(ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * Overloaded method for updating a activities_users table
     *
     * @param usersActivityId
     * @param mail
     * @param activityName
     * @param status
     * @param start
     * @param end
     */

    public void update(long usersActivityId, String mail, String activityName, int status, Timestamp start, Timestamp end) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            LOGGER.trace("Get con");

            pstmt = con.prepareStatement(SQL__TRANSACTION_ACTIVITY_USERS_UPDATE);
            pstmt.setTimestamp(1, start);
            pstmt.setTimestamp(2, end);
            pstmt.setInt(3, status);
            pstmt.setLong(4, usersActivityId);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            LOGGER.error(ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * Overloaded method for updating a activities_users table
     *
     * @param userId
     * @param activityId
     * @param start
     * @param end
     */
    public void update(long userId, long activityId, Timestamp start, Timestamp end) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();

            pstmt = con.prepareStatement(SQL__ACTIVITY_USERS_UPDATE);
            pstmt.setTimestamp(1, start);
            pstmt.setTimestamp(2, end);
            pstmt.setLong(3, userId);
            pstmt.setLong(4, activityId);
            pstmt.executeUpdate();
            pstmt.close();

        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            LOGGER.error(ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * Overloaded method for updating a activities_users table
     *
     * @param userId
     * @param activityId
     * @param status
     */
    public void update(long userId, long activityId, Integer status) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();

            pstmt = con.prepareStatement(SQL__ACTIVITY_USERS_UPDATE_STATUS);
            pstmt.setInt(1, status);
            pstmt.setLong(2, userId);
            pstmt.setLong(3, activityId);
            pstmt.executeUpdate();
            pstmt.close();

        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            LOGGER.error("Error while changing the status--> +" + ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }

    }

    /**
     * delete the record in activities_users table
     *
     * @param userActivityId
     */
    public void delete(long userActivityId) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            LOGGER.trace("Get con");

            pstmt = con.prepareStatement(SQL__TRANSACTION_ACTIVITY_USERS_DELETE);
            pstmt.setLong(1, userActivityId);
            pstmt.executeUpdate();
            pstmt.close();

        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            LOGGER.error(ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * get primary key and date from the activities_users table
     *
     * @param userId
     * @param activityId
     * @return
     */
    public Request getUsersActivityId(long userId, long activityId) {
        Connection con = null;
        Request result = new Request();
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;

            preparedStatement = con.prepareStatement(SQL__GET_USERS_ACTIVITY_ID);
            preparedStatement.setLong(1, userId);
            preparedStatement.setLong(2, activityId);
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            result.setId(resultSet.getLong(1));
            result.setStart(resultSet.getTimestamp(2));
            result.setEnd(resultSet.getTimestamp(3));
            preparedStatement.close();
            resultSet.close();

        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            LOGGER.error(ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return result;
    }
}
