package com.epam.rd.khranovskyi.web;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;

public class FooterTag extends SimpleTagSupport {
    StringWriter stringWriter = new StringWriter();

    public void doTag()

            throws JspException, IOException {
        getJspBody().invoke(stringWriter);
        getJspContext().getOut().println(stringWriter.toString());
    }
}
