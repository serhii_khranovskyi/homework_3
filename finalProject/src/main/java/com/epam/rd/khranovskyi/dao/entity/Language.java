package com.epam.rd.khranovskyi.dao.entity;

public enum Language {
    ENGLISH(2), RUSSIAN(1);
    private int code;

    Language(int code) {
        this.code = code;
    }


    public int getCode() {
        return code;
    }

    public String getName() {
        return name().toLowerCase();
    }

    public static Language get(String name) {
        if (ENGLISH.getName().equals(name.toLowerCase())) return ENGLISH;
        return RUSSIAN;
    }
}
