package com.epam.rd.khranovskyi.dao;

import com.epam.rd.khranovskyi.dao.entity.User;
import org.apache.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class EmailSender {
    private static final Logger LOGGER = Logger.getLogger(EmailSender.class);

    /**
     * Send a letter to user mail after changing the profile
     *
     * @param to       for whom
     * @param user     user info
     * @param language default language
     * @return
     */

    public static boolean sendMail(String to, User user, String language) {

        Properties properties = getProperties();

        // default session
        Session session = Session.getDefaultInstance(properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        // Specify the Username and the PassWord
                        return new PasswordAuthentication(
                                "yicrygdd@gmail.com", "1234567890V");
                    }
                });
        try {// email message
            MimeMessage message = new MimeMessage(session);

            setMailConfig(message, to, user, language);


            // Send message
            Transport.send(message);
            System.out.println("Email Sent successfully....");
        } catch (MessagingException mex) {
            LOGGER.trace("Error while sending email -->" + mex);
            return false;
        }
        return true;
    }

    private static Properties getProperties() {
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.socketFactory.port", 465);
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", 465);
        return properties;
    }

    public static final void setMailConfig(Message message, String to, User user, String language) throws MessagingException {
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        // subject line
        message.setSubject("Me.corp");
        // actual mail body
        message.setText(textLocale(user, language));
    }

    /**
     * @param user
     * @param language
     * @return the specified text for the definite language
     */
    private static final String textLocale(User user, String language) {
        if (language.equals("ru")) {
            return "Привет, " + user.getFirst_name() + ". Это письмо было создано автоматически Me, Вам не нужно на него отвечать." +
                    "Ваши персональные данные были изменены. Если у Вас есть вопросы, напишите нам, пожалуйста. Наша почта odeosat@gmail.com" + System.lineSeparator() +
                    "Хорошего дня!";
        }
        return "Hello, " + user.getFirst_name() + ". This letter was created automatically by the Me, you do not need to reply to it." +
                "Your personal data was changed. If you have any questions, please, ask us. Here is our mail odeosat@gmail.com" + System.lineSeparator() +
                "Have a nice day!";
    }

}
