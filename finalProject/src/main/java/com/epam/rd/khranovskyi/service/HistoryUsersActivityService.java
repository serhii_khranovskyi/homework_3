package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.ActivityDAO;
import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.bean.UserActivityBean;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.util.List;

@Service
public class HistoryUsersActivityService {
    private static final Logger LOG = Logger.getLogger(HistoryUsersActivityService.class);

    public String execute(Model model, WebRequest request) throws IOException {

        String language = (String) model.getAttribute("defaultLocale");
        LOG.trace("Language -->" + language);
        Long userId = (Long) model.getAttribute("userId");

        String activityId = request.getParameter("activityId");
        LOG.trace("Id -- >" + userId);
        List<UserActivityBean> userActivityBeans = null;

        ActivityDAO activityDAO = new ActivityDAO();

        if (language.equals("en")) {
            //Get User`s history about the activity in English
            userActivityBeans = activityDAO.getActivityBeansHistoryTranslation(userId, Long.parseLong(activityId), "English");
        } else if (language.equals("ru")) {
            //Get User`s history about the activity
            userActivityBeans = activityDAO.getActivityBeansHistory(userId, Long.parseLong(activityId));
        }
        LOG.trace("History-->" + userActivityBeans);
        request.setAttribute("userActivityBeans", userActivityBeans, RequestAttributes.SCOPE_REQUEST);
        return Path.PAGE__LIST_USERS_ACTIVITY_HISTORY;
    }
}
