package com.epam.rd.khranovskyi.dao;

import com.epam.rd.khranovskyi.dao.entity.Language;
import com.epam.rd.khranovskyi.dao.entity.Role;
import com.epam.rd.khranovskyi.dao.entity.User;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {
    private static final Logger LOGGER = Logger.getLogger(UserDAO.class);

    private static final String SQL__FIND_USER_BY_MAIL =
            "SELECT id_users, mail, first_name, last_name, password, rolecol, name  FROM users JOIN role ON role_id_role=id_role JOIN language ON language_id_language = id_language  WHERE mail=?;";

    private static final String SQL__CHECK_USER_BY_MAIL =
            "SELECT EXISTS(SELECT mail FROM users WHERE mail = ?)";

    private static final String SQL__FIND_USER_BY_ID =
            "SELECT * FROM users WHERE id_users=?";


    private static final String SQL_INSERT_USER =
            "INSERT users(mail,first_name,last_name,users.password,role_id_role,language_id_language) " +
                    "values (?,?,?,?,?,?);";

    private static final String SQL_UPDATE_USER =
            "UPDATE users SET password=?, first_name=?, last_name=?, mail=?,role_id_role=?,language_id_language=?" +
                    "	WHERE id_users=?";
    private static final String SQL_UPDATE_USER_LANGUAGE = "update users SET language_id_language =? where id_users = ?;";

    private static final String SQL_DELETE_USER =
            "DELETE FROM users WHERE mail = ?;";

    private static final String SQL__SELECT_ALL_INFO_ABOUT_USER =
            "select id_users, mail, first_name, last_name, password, \n" +
                    "COUNT(activities_users.activities_id_activities) as activities_count, \n" +
                    "SUM(ending - beginning) as totaltime\n" +
                    "from users \n" +
                    "left join activities_users on id_users = activities_users.users_id_users\n" +
                    "where status =2 group by mail;";

    private static final String SQL__USER_SET_TIME = "update activities_users SET beginning = ?, ending = ?" +
            " where users_id_users = '?' AND activities_id_activities = '?';";
    private static final String SQL__GET_USERS = "select id_users, mail,first_name,last_name,password, rolecol, name " +
            "from users join role on role_id_role=id_role join language on language_id_language=id_language;";

    private static final String SQL__FIND_ID_BY_MAIL = "select id_users from users where mail=?";

    private static final String SQL__USER_UPDATE_PROFILE = "update users set mail = ?, first_name= ?, last_name = ?, password = ?  where id_users = ?;";

    /**
     * Returns a user with the given identifier.
     *
     * @param id User identifier.
     * @return User entity.
     */
    public User findUser(Long id) {
        User user = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            ProfileMapper mapper = new ProfileMapper();
            pstmt = con.prepareStatement(SQL__FIND_USER_BY_ID);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next())
                user = mapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return user;
    }

    /**
     * insert the user into the data base
     *
     * @param user
     */
    public void insertUser(User user) {

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            LOGGER.info("user mail--> " + user.getMail() + " userFIO--> " + user.getFirst_name() + " " + user.getLast_name() + " Password--> " + user.getPassword() + "user role/language" + user.getRole().getCode() + " " + user.getLanguage().getCode());
            PreparedStatement p = con.prepareStatement("Select id_role from role where rolecol = ?");
            p.setInt(1, user.getRole().getCode());
            rs = p.executeQuery();
            rs.next();
            int roleId = rs.getInt("id_role");
            pstmt = con.prepareStatement(SQL_INSERT_USER);
            pstmt.setString(1, user.getMail());
            pstmt.setString(2, user.getFirst_name());
            pstmt.setString(3, user.getLast_name());
            pstmt.setString(4, user.getPassword());
            pstmt.setInt(5, roleId);
            pstmt.setInt(6, user.getLanguage().getCode());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            LOGGER.error(ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
            LOGGER.info("Method insert ends");
        }
    }

    /**
     * Delete the user by mail
     *
     * @param mail
     */
    public void deleteUser(String mail) {
        PreparedStatement pstmt = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_DELETE_USER);
            pstmt.setString(1, mail);
            pstmt.execute();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            LOGGER.error(ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * Returns a user with the given login.
     *
     * @param mail User login.
     * @return User entity.
     */
    public User findUserByMail(String mail) {
        LOGGER.trace("Start find in DAO");
        User user = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            UserMapper mapper = new UserMapper();
            pstmt = con.prepareStatement(SQL__FIND_USER_BY_MAIL);
            pstmt.setString(1, mail);
            rs = pstmt.executeQuery();
            if (rs.next())
                user = mapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return user;
    }

    /**
     * checking if the user`s mail exist in db
     *
     * @param mail
     * @return
     */
    public boolean checkUser(String mail) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        boolean isNewUser = false;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__CHECK_USER_BY_MAIL);
            pstmt.setString(1, mail);
            rs = pstmt.executeQuery();
            int result = rs.getInt(1);
            if (result == 0) isNewUser = true;
            rs.close();
            pstmt.close();
        } catch (Exception ex) {
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return isNewUser;
    }

    /**
     * Set a new language parameter into db
     *
     * @param userId
     * @param language
     */
    public void updateUserLanguage(Long userId, int language) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL_UPDATE_USER_LANGUAGE);

            pstmt.setInt(1, language);
            pstmt.setLong(2, userId);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            LOGGER.error("error--> " + ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * Update user.
     *
     * @param user user to update.
     */
    public void updateUser(User user) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            updateUser(con, user);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            LOGGER.error("error--> " + ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * update the table User
     *
     * @param user
     */
    public void updateUserProfile(User user) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement update = con.prepareStatement(SQL__USER_UPDATE_PROFILE);
            update.setString(1, user.getMail());
            update.setString(2, user.getFirst_name());
            update.setString(3, user.getLast_name());
            update.setString(4, user.getPassword());
            update.setLong(5, user.getId());

            update.executeUpdate();

        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            LOGGER.trace("Error -->" + ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * Update user.
     *
     * @param user user to update.
     * @throws SQLException
     */

    public void updateUser(Connection con, User user) throws SQLException {
        PreparedStatement preparedStatement = con.prepareStatement(SQL__FIND_ID_BY_MAIL);
        preparedStatement.setString(1, user.getMail());
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        int userId = rs.getInt("id_users");
        rs.close();
        preparedStatement.close();
        PreparedStatement preparedStatement1 = con.prepareStatement("select id_role from role where rolecol=?");

        preparedStatement1.setInt(1, user.getRole().getCode());
        ResultSet rs1 = preparedStatement1.executeQuery();
        rs1.next();
        int roleId = rs1.getInt("id_role");
        PreparedStatement pstmt = con.prepareStatement(SQL_UPDATE_USER);
        int k = 1;
        pstmt.setString(k++, user.getPassword());
        pstmt.setString(k++, user.getFirst_name());
        pstmt.setString(k++, user.getLast_name());
        pstmt.setString(k++, user.getMail());
        pstmt.setInt(k++, roleId);
        pstmt.setInt(k++, user.getLanguage().getCode());
        pstmt.setLong(k++, userId);
        pstmt.executeUpdate();
        pstmt.close();
    }

    /**
     * Get users information
     *
     * @return the list of users
     */
    public List<User> getUsersBeans() {
        List<User> userList = new ArrayList<User>();
        Statement stmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            LOGGER.trace("Get connection");
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL__GET_USERS);
            UserDAO.UserMapper mapper = new UserDAO.UserMapper();
            while (rs.next())
                userList.add(mapper.mapRow(rs));
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            LOGGER.error(ex);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return userList;
    }

    /**
     * Get all statistic users` information
     *
     * @return
     */
    public ArrayList<User> getUsersInfo() {
        ArrayList<User> userList = new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            UserDAO.UserStatisticMapper mapper = new UserDAO.UserStatisticMapper();
            pstmt = con.prepareStatement(SQL__SELECT_ALL_INFO_ABOUT_USER);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                userList.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            LOGGER.error("Error -->" + ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return userList;
    }

    /**
     * Extracts a user from the result set row.
     */
    private static class UserMapper implements EntityMapper<User> {

        @Override
        public User mapRow(ResultSet rs) {
            try {
                User user = new User();
                user.setId(rs.getLong(Fields.USER__ID));
                user.setMail(rs.getString(Fields.USER__MAIL));
                user.setPassword(rs.getString(Fields.USER__PASSWORD));
                user.setFirst_name(rs.getString(Fields.USER__FIRST_NAME));
                user.setLast_name(rs.getString(Fields.USER__LAST_NAME));
                user.setLanguage(Language.get(rs.getString("name")));
                user.setRole(Role.get(rs.getInt("rolecol")));
                return user;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Extracts a user`s statistic from the result set row.
     */
    private static class UserStatisticMapper implements EntityMapper<User> {

        @Override
        public User mapRow(ResultSet rs) {
            try {
                User user = new User();
                user.setId(rs.getLong(Fields.USER__ID));
                user.setMail(rs.getString(Fields.USER__MAIL));
                user.setFirst_name(rs.getString(Fields.USER__FIRST_NAME));
                user.setLast_name(rs.getString(Fields.USER__LAST_NAME));
                user.setPassword(rs.getString(Fields.USER__PASSWORD));
                user.setActivitiesCount(rs.getInt(6));
                long millis = rs.getLong(7);
                long seconds = (millis / 1000) % 60;
                long minutes = ((millis - seconds) / 1000) / 60;
                long hours = minutes / 60;
                user.setAllActivitiesTime(hours);
                return user;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Extracts a user profile information from the result set row.
     */
    private static class ProfileMapper implements EntityMapper<User> {

        @Override
        public User mapRow(ResultSet rs) {
            try {
                User user = new User();
                user.setId(rs.getLong(Fields.USER__ID));
                user.setMail(rs.getString(Fields.USER__MAIL));
                user.setPassword(rs.getString(Fields.USER__PASSWORD));
                user.setFirst_name(rs.getString(Fields.USER__FIRST_NAME));
                user.setLast_name(rs.getString(Fields.USER__LAST_NAME));
                return user;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

}
