package com.epam.rd.khranovskyi.dao.bean;

import java.sql.Timestamp;

public class UserBean {

    private long userActivityId;

    private long activityId;

    private String activityName;

    private long userId;

    private String userLogin;

    private Timestamp start;

    private Timestamp end;

    private int status;

    public long getUserActivityId() {
        return userActivityId;
    }

    public void setUserActivityId(long userActivityId) {
        this.userActivityId = userActivityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public long getActivityId() {
        return activityId;
    }

    public void setActivityId(long activityId) {
        this.activityId = activityId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "UserBean = [activityId = " + activityId + ", activity name = " + activityName + ", userId = " +
                userId + ", login = " + userLogin + ", Start = " + start + ", end = " + end + ", status =" + status;
    }
}
