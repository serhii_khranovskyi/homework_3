package com.epam.rd.khranovskyi.dao.bean;

import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

public class SessionModel {
    private static Model model;

    public static Model createModel() {
        model = new ExtendedModelMap();
        return model;

    }

    public static Model getModel() {
        return model;
    }

    public static void deleteModel() {
        model = null;
    }
}
