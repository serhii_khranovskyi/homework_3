package com.epam.rd.khranovskyi.service;


import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.UserDAO;
import com.epam.rd.khranovskyi.dao.entity.User;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;

@Service
public class ProfileService {
    private static final Logger LOG = Logger.getLogger(ProfileService.class);

    public String execute(Model model, WebRequest request) throws IOException {

        LOG.trace("Get https session");
        String language = (String) model.getAttribute("defaultLocale");
        LOG.trace("defaultLocale -->" + language);
        Long userId = (Long) model.getAttribute("userId");
        LOG.trace("Id -- >" + userId);
        //Get the user information
        User user = new UserDAO().findUser(userId);
        LOG.trace("Found in DB: userId --> " + user.getId() + " mail-->" + user.getMail());

        request.setAttribute("user", user, RequestAttributes.SCOPE_REQUEST);
        LOG.trace("Set the request attribute: userid --> " + +user.getId() + " mail-->" + user.getMail());
        LOG.debug("Commands finished");

        return Path.PAGE__PROFILE;
    }
}
