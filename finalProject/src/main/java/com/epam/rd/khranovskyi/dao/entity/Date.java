package com.epam.rd.khranovskyi.dao.entity;

public class Date {
    private String start;
    private String end;

    public Date(String start, String end) {
        this.start = start;
        this.end = end;
    }

    public Date() {
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Date[start = ").append(start).append("; end = ").append(end).append(" ]");
        return sb.toString();
    }
}
