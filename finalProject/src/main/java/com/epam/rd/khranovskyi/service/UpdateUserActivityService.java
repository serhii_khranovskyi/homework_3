package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.RequestDAO;
import com.epam.rd.khranovskyi.dao.entity.Request;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

@Controller
public class UpdateUserActivityService {
    private static final Logger LOG = Logger.getLogger(UpdateUserActivityService.class);

    public String execute(Model model, WebRequest request) throws IOException {
        String language = (String) model.getAttribute("defaultLocale");
        LOG.trace("Language -->" + language);
        Long userId = (Long) model.getAttribute("userId");
        LOG.trace("Id -- >" + userId);
        String activityId = request.getParameter("activityId");
        LOG.trace("Activity ID-->" + activityId);

        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());


        Request getPeriodOfUserActivity = new RequestDAO().getUsersActivityId(userId, Long.parseLong(activityId));

        LOG.trace("start " + getPeriodOfUserActivity.getStart() + " end " + getPeriodOfUserActivity.getEnd());

        //if the start cell is filled, it will be filled the second one
        if (getPeriodOfUserActivity.getStart().toString().equals("1999-12-31 23:00:00.0")) {
            new RequestDAO().updateStart(getPeriodOfUserActivity.getId(), timestamp);
        } else if (getPeriodOfUserActivity.getEnd().toString().equals("1999-12-31 23:00:00.0")) {
            new RequestDAO().updateEnd(getPeriodOfUserActivity.getId(), timestamp);
            new RequestDAO().insert(userId, Long.parseLong(activityId), 2);
        } else {
            new RequestDAO().updateEnd(getPeriodOfUserActivity.getId(), timestamp);
            new RequestDAO().insert(userId, Long.parseLong(activityId), 2);
            new RequestDAO().updateStart(getPeriodOfUserActivity.getId(), timestamp);
        }
        return "redirect:" + Path.COMMAND__LIST_USERS_ACTIVITIES;
    }
}
