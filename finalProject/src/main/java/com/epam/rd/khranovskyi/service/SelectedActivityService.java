package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.RequestDAO;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;

@Service
public class SelectedActivityService {
    private static final Logger LOG = Logger.getLogger(SelectedActivityService.class);

    public String execute(Model model, WebRequest request) throws IOException {

        Long userId = (Long) model.getAttribute("userId");
        LOG.trace("userId--> " + userId);

        String activityId = request.getParameter("activityId");
        LOG.trace("Parameter--> " + activityId);

        //insert the record into activities_users table with status ("waiting for confirmation")
        new RequestDAO().insert(userId, Long.parseLong(activityId), 1);
        LOG.debug("Service finished");

        return "redirect:" + Path.COMMAND__SELECTED_CATEGORY;
    }
}
