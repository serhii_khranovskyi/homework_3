package com.epam.rd.khranovskyi.web.controller;

import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.bean.SessionModel;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;


@Controller
public class LanguageController {
    private static final Logger LOG = Logger.getLogger(LanguageController.class);
    private Model model;

    public LanguageController() {
        if (SessionModel.getModel() == null) {
            model = SessionModel.createModel();
        } else {
            model = SessionModel.getModel();
        }
    }

    @RequestMapping("/language")
    public String loginHandle(WebRequest webRequest) {
        model.addAttribute("defaultLocale", webRequest.getParameter("language"));
        return Path.PAGE__LOGIN;
    }
}
