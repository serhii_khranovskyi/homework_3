package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.RequestDAO;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.sql.Timestamp;

public class ActivityUsersModify {
    private static final Logger LOG = Logger.getLogger(ActivityUsersModify.class);


    public String execute(Model model, WebRequest webRequest) throws IOException {

        String combobox = webRequest.getParameter("combobox");
        LOG.trace("Request parameter: combobox -->" + combobox);

        String activityUserId = webRequest.getParameter("id");
        LOG.trace("Request parameter: activity_name --> " + activityUserId);

        String activity_name = webRequest.getParameter("activity_name");
        LOG.trace("Request parameter: activity_name --> " + activity_name);

        String user_name = webRequest.getParameter("user_name");
        LOG.trace("Request parameter: user_name --> " + user_name);

        String status = webRequest.getParameter("status");
        LOG.trace("Request parameter: status --> " + status);

        String start_date = webRequest.getParameter("start_date");
        LOG.trace("Request parameter: start_date --> " + start_date);

        String end_date = webRequest.getParameter("end_date");
        LOG.trace("Request parameter: end_date--> " + end_date);

        String errorMessage = null;
        String forward = Path.PAGE__ERROR_PAGE;

        String language = (String) model.getAttribute("defaultLocale");
        LOG.trace("Language -->" + language);

        if (activity_name == null || user_name == null ||
                status == null || start_date == null ||
                end_date == null) {
            errorMessage = "Parameters cannot be empty";
            webRequest.setAttribute("errorMessage", errorMessage, RequestAttributes.SCOPE_REQUEST);
            LOG.error("errorMessage --> " + errorMessage);
            return forward;
        }
        LOG.info("Check the activity_users");
        if (combobox.equals("0")) {
            //insert a new record into the activities_users table
            new RequestDAO().insert(user_name, activity_name, Integer.parseInt(status), Timestamp.valueOf(start_date.replace("T", " ").concat(":00")), Timestamp.valueOf(end_date.replace("T", " ").concat(":00")), language);
        }
        if (combobox.equals("1")) {
            //update the record in the activities table
            new RequestDAO().update(Long.parseLong(activityUserId), user_name, activity_name, Integer.parseInt(status), Timestamp.valueOf(start_date.replace("T", " ").concat(":00")), Timestamp.valueOf(end_date.replace("T", " ").concat(":00")));
        }
        if (combobox.equals("2")) {
            //delete the record
            new RequestDAO().delete(Long.parseLong(activityUserId));
        }


        return "redirect:" + Path.COMMAND__LIST_USERS;
    }
}
