package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.UserDAO;
import com.epam.rd.khranovskyi.dao.entity.Role;
import com.epam.rd.khranovskyi.dao.entity.User;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;


@Service
public class LoginService {
    private static final Logger LOG = Logger.getLogger(LoginService.class);

    public String execute(Model model, WebRequest webRequest) throws IOException {
        LOG.debug("Service starts");

        // HttpSession session = request.getSession();
        String userPreferenceLocale = (String) model.getAttribute("defaultLocale");

        LOG.trace("User preference locale -->" + userPreferenceLocale);

        LOG.trace("Language-->" + webRequest.getParameter("language"));
        // obtain login and password from the request
        String login = webRequest.getParameter("login");
        LOG.trace("Request parameter: login --> " + login);


        String password = webRequest.getParameter("password");
        LOG.trace("Request parameter: password --> " + password);
        // error handler
        String errorMessage = null;
        String forward = Path.PAGE__ERROR_PAGE;

        if (login == null || password == null || login.isEmpty() || password.isEmpty()) {
            errorMessage = "Login/password cannot be empty";
            webRequest.setAttribute("errorMessage", errorMessage, WebRequest.SCOPE_REQUEST);
            LOG.error("errorMessage --> " + errorMessage);
            return forward;
        }
        LOG.info("Find user");
        //get User information
        User user = new UserDAO().findUserByMail(login);


        if (user == null || !password.equals(user.getPassword())) {
            errorMessage = "Cannot find user with such login/password";
            webRequest.setAttribute("errorMessage", errorMessage, WebRequest.SCOPE_REQUEST);

            return forward;
        } else {
            int userRole = user.getRole().getCode();
            LOG.trace("userRole --> " + userRole + " && " + Role.ADMIN.getCode() + " user.getrole -->" + user.getRole().getCode());

            model.addAttribute("user", user);
            LOG.trace("Set the session attribute: user --> " + user);

            model.addAttribute("userId", user.getId());
            LOG.trace("Set the session attribute: userId --> " + user.getId());

            model.addAttribute("userRole", userRole);
            LOG.trace("Set the session attribute: userRole --> " + userRole);

            LOG.info("User " + user + " logged as " + Role.get(userRole));

            // work with i18n
            String userLocaleName = user.getLanguage().getName();
            LOG.trace("userLocalName --> " + userLocaleName);

            if (userLocaleName != null && !userLocaleName.isEmpty()) {
                //update user language

                if (userLocaleName.equals("russian"))
                    userLocaleName = "ru";
                if (userLocaleName.equals("english"))
                    userLocaleName = "en";

                if (!userLocaleName.equals(userPreferenceLocale) && userPreferenceLocale != null) {
                    userLocaleName = userPreferenceLocale;
                    if (userLocaleName.equals("ru"))
                        new UserDAO().updateUserLanguage(user.getId(), 1);
                    if (userLocaleName.equals("en"))
                        new UserDAO().updateUserLanguage(user.getId(), 2);
                }
                model.addAttribute("javax.servlet.jsp.jstl.fmt.locale", userLocaleName);
                model.addAttribute("defaultLocale", userLocaleName);
                LOG.trace("Set the session attribute: defaultLocaleName --> " + userLocaleName);

                LOG.info("Locale for user: defaultLocale --> " + userLocaleName);
                LOG.trace("forward--> " + forward);

                if (userRole == Role.ADMIN.getCode())
                    return new ListUsersService().execute(model, webRequest);

                if (userRole == Role.USER.getCode())
                    return new ListCategoriesService().execute(model, webRequest);
            }
        }
        return forward;
    }
}
