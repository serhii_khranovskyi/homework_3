<%@ page language="java" %>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="List users" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/register_style.jspf" %>


<body>
<nav class="navbar navbar-inverse" style=" background-color: #92D3E8;">
    <ul>
        <li style="margin: 30px;padding-left: 350px"><a
                href="/accounting_time/table?command=activitiesTable"><fmt:message
                key="list_users_jsp.table.header.activity"/></a></li>
        <li style="margin: 30px;"><a href="/accounting_time/table?command=categoriesTable"> <fmt:message
                key="users_activities_jsp.head.link.categories"/></a></li>
        <li style="margin: 30px;"><a href="/accounting_time/table?command=usersTable"><fmt:message
                key="list_users_jsp.table.header.user"/></a></li>
        <li style="margin: 30px;padding-right: 350px"><a href="/accounting_time/logout"><fmt:message
                key="list_users_jsp.table.header.logout"/></a></li>
        <li class="helper"></li>
    </ul>
</nav>

<table id="main-container">
    <tr>

        <td class="insert update delete activity_user">

            <form id="modify_activity_user" action="modify" method="post">

                <input type="hidden" name="command" value="modifyActivityUser"/>

                <fieldset>
                    <legend>
                        <fmt:message key="list_users_jsp.label.content.modifyActivityUser"/>
                    </legend>
                    <select name="combobox">
                        <option value="0"><fmt:message key="list_users_jsp.body.combobox.insert"/></option>
                        <option value="1"><fmt:message key="list_users_jsp.body.combobox.update"/></option>
                        <option value="2"><fmt:message key="list_users_jsp.body.combobox.delete"/></option>
                    </select>

                    <br/> <br/>

                    <legend>

                        <fmt:message key="categories_jsp.table.header.category_id"/>
                        <input type="text" name="id"/>
                        <br/> <br/>

                        <fmt:message key="list_users_jsp.label.content.activityName"/>
                        <input type="text" name="activity_name"/>
                        <br/> <br/>


                        <fmt:message key="list_users_jsp.label.content.userMail"/>

                        <input type="text" name="user_name"/>
                        <br/> <br/>

                        <fmt:message key="list_users_jsp.label.content.status"/>

                        <input type="text" name="status"/>
                        <br/> <br/>

                        <fmt:message key="list_users_jsp.label.content.startDate"/>

                        <input type="datetime-local" name="start_date"/>
                        <br/> <br/>


                        <fmt:message key="list_users_jsp.label.content.endDate"/>

                        <input type="datetime-local" name="end_date"/>
                        <br/> <br/>

                    </legend>
                </fieldset>
                <br/> <br/>

                <input type="submit" value='<fmt:message key="list_users_jsp.button.applyActivityUser"/>'>
            </form>
            <%-- CONTENT --%>

        </td>
    </tr>
</table>
<div style="width: 100%; overflow: auto; height: 250px;">
    <table style="width: 90%; margin: auto">
        <tr>
            <td class="content">
                <%-- TABLE --%>

                <c:choose>
                    <c:when test="${fn:length(userBeanList) == 0}">No such table</c:when>

                    <c:otherwise>
                        <table id="list_order_table" class="data" border="1">
                            <thead class="data">
                            <tr>
                                <td><fmt:message key="categories_jsp.table.header.category_id"/></td>
                                <td><fmt:message key="list_users_jsp.table.header.activity"/></td>
                                <td><fmt:message key="list_users_jsp.table.header.user"/></td>
                                <td><fmt:message key="list_users_jsp.table.header.dateStart"/></td>
                                <td><fmt:message key="list_users_jsp.table.header.dateEnd"/></td>
                                <td><fmt:message key="list_users_jsp.table.header.status"/></td>
                            </tr>
                            </thead>
                            <tbody class="data">
                            <c:forEach var="bean" items="${userBeanList}">

                                <tr>
                                    <td>${bean.userActivityId}</td>
                                    <td>${bean.activityName}</td>
                                    <td>${bean.userLogin}</td>
                                    <td>${bean.start}</td>
                                    <td>${bean.end}</td>
                                    <td>${bean.status}</td>
                                </tr>

                            </c:forEach>
                            </tbody>
                        </table>
                    </c:otherwise>
                </c:choose>

                <%-- CONTENT --%>
            </td>
        </tr>

    </table>
</div>
<div style="text-align: center">
    <%@ include file="/WEB-INF/jspf/footer.jspf" %>
</div>
</body>
</html>
