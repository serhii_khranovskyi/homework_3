<%@ page language="java" %>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Profile" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<%@ include file="/WEB-INF/jspf/directive/register_style.jspf" %>


<body>
<nav class="navbar navbar-inverse" style=" background-color: #92D3E8;">
    <ul>
        <li style="padding-left: 350px; margin: 30px;"><a href="/accounting_time/user?command=myActivitiesCommand">
            <fmt:message key="users_main_page_jsp.table.button.my_activities"/></a></li>
        <li style="margin: 30px"><a href="/accounting_time/user?command=users_main_page"><fmt:message
                key="users_activities_jsp.head.link.categories"/></a></li>
        <li style="padding-right: 350px; margin: 30px"><a href="/accounting_time/logout"><fmt:message
                key="list_users_jsp.table.header.logout"/></a></li>
        <li class="helper"></li>
    </ul>
</nav>
<br/><br/>
<div class="container">
    <div class="row main-form">
        <form id="registration_form" action="user" method="post">
            <div class="form-group">
                <label for="name" class="cols-sm-2 control-label"><fmt:message
                        key="registration.label.content.firstname"/></label>
                <div class="cols-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                        <input type="text" class="form-control" name="first_name" id="name" value="${user.first_name}"
                               placeholder="<fmt:message key="registration.label.content.firstname"/>"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="cols-sm-2 control-label"><fmt:message
                        key="registration.label.content.lastname"/></label>
                <div class="cols-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                        <input type="text" class="form-control" name="last_name" id="username" value="${user.last_name}"
                               placeholder="<fmt:message key="registration.label.content.lastname"/>"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="cols-sm-2 control-label"><fmt:message
                        key="registration.label.content.mail"/></label>
                <div class="cols-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                        <input type="text" class="form-control" name="mail" id="email" value="${user.mail}"
                               placeholder="<fmt:message key="registration.label.content.mail"/>"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="password" class="cols-sm-2 control-label"><fmt:message
                        key="registration.label.content.password"/></label>
                <div class="cols-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                        <input type="password" class="form-control" name="password" id="password"
                               value="${user.password}"
                               placeholder="<fmt:message key="registration.label.content.password"/>"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="confirm" class="cols-sm-2 control-label"><fmt:message
                        key="registration.label.content.rewritePassword"/></label>
                <div class="cols-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                        <input type="password" class="form-control" name="password1" id="confirm"
                               value="${user.password}"
                               placeholder="<fmt:message key="registration.label.content.rewritePassword"/>"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <input type="hidden" name="command" value="editProfile"/>
                <input type="submit" id="button" class="btn btn-primary btn-lg btn-block login-button"
                       value="<fmt:message key="profile_jsp.edit"/>">
            </div>
        </form>
    </div>
</div>
<br/><br/>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
