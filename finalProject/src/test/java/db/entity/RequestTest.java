package db.entity;
import com.epam.rd.khranovskyi.dao.entity.Request;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RequestTest {
    @Test
    void requestToString(){
        Request request = new Request();
        request.setUserName("name");
        request.setActivityName("activity");
        request.setStart(Timestamp.valueOf("1000-01-01 00:01:00.0"));
        request.setEnd(Timestamp.valueOf("1000-01-01 00:01:00.0"));
        assertEquals("Request{" + "activityName='" + "activity" + '\'' +
                ", userName='" + "name" + '\'' +
                ", start=" +"1000-01-01 00:01:00.0" +
                ", end=" + "1000-01-01 00:01:00.0" +
                '}', request.toString());
    }

    @Test
    void setActivityName(){
        Request request = new Request();
        request.setActivityName("test");
        assertEquals("test", request.getActivityName());
    }

    @Test
    void getActivityName(){
        Request request = new Request();
        request.setActivityName("test");
        assertEquals("test", request.getActivityName());
    }

    @Test
    void setUserName(){
        Request request = new Request();
        request.setUserName("test");
        assertEquals("test", request.getUserName());
    }

    @Test
    void getUserName(){
        Request request = new Request();
        request.setUserName("test");
        assertEquals("test", request.getUserName());
    }

    @Test
    void getEnd() {
        Request request = new Request();
        request.setEnd(Timestamp.valueOf("1000-01-01 00:01:00.0"));
        assertEquals(Timestamp.valueOf("1000-01-01 00:01:00.0"),request.getEnd());
    }

    @Test
    void setEnd() {
        Request request = new Request();
        request.setEnd(Timestamp.valueOf("1000-01-01 00:01:00.0"));
        assertEquals(Timestamp.valueOf("1000-01-01 00:01:00.0"),request.getEnd());
    }

    @Test
    void setStart() {
        Request request = new Request();
        request.setStart(Timestamp.valueOf("1000-01-01 00:01:00.0"));
        assertEquals(Timestamp.valueOf("1000-01-01 00:01:00.0"),request.getStart());
    }

    @Test
    void getStart() {
        Request request = new Request();
        request.setStart(Timestamp.valueOf("1000-01-01 00:01:00.0"));
        assertEquals(Timestamp.valueOf("1000-01-01 00:01:00.0"),request.getStart());
    }
}
