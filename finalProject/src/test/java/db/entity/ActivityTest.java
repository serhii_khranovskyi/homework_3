package db.entity;

import com.epam.rd.khranovskyi.dao.entity.Activity;
import com.epam.rd.khranovskyi.dao.entity.Category;
import com.epam.rd.khranovskyi.dao.entity.Date;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ActivityTest {

    @Test
    void createActivityTest(){
        Activity activity = Activity.createActivity("Test", new Category("tet"));
        Activity expected = new Activity();
        expected.setName("Test");
        Category expectedCategory = new Category("tet");
        expected.setCategory(expectedCategory);
        assertEquals(expected.toString(),activity.toString());
    }

    @Test
    void setNameTest(){
        Activity activity = new Activity();
        activity.setName("Test");
        assertEquals("Test",activity.getName());
    }

    @Test
    void getNameTest(){
        Activity activity = new Activity();
        activity.setName("Test");
        assertEquals("Test",activity.getName());
    }

    @Test
    void getId(){
        Activity activity = new Activity();
        activity.setId(1L);
        assertEquals(1L,activity.getId());
    }

    @Test
    void setCategoryTest(){
        Activity activity = new Activity();
        activity.setCategory(new Category("test"));
        assertEquals("test",activity.getCategory().getName());
    }

    @Test
    void getCategoryTest(){
        Activity activity = new Activity();
        activity.setCategory(new Category("test"));
        assertEquals("test",activity.getCategory().getName());
    }

    @Test
    void setDate(){
        Activity activity = new Activity();
        Date date = new Date();
        date.setStart("1000.01.01 00:01");
        activity.setDate(date);
        assertEquals("1000.01.01 00:01",activity.getDate().getStart());
    }

    @Test
    void getDate(){
        Activity activity = new Activity();
        Date date = new Date();
        date.setStart("1000.01.01 00:01");
        activity.setDate(date);
        assertEquals("1000.01.01 00:01",activity.getDate().getStart());
    }

    @Test
    void getLocale(){
        Activity activity = new Activity();
        activity.setLocale(1);
        assertEquals(1,activity.getLocale());
    }

    @Test
    void setLocale(){
        Activity activity = new Activity();
        activity.setLocale(1);
        Activity expected = new Activity();
        expected.setLocale(1);
        assertEquals(expected.getLocale(),activity.getLocale());
    }

    @Test
    void getTranslation(){
        Activity activity = new Activity();
        activity.setNameTranslation("test");
        assertEquals("test",activity.getNameTranslation());
    }


    @Test
    void getStatus(){
        Activity activity = new Activity();
        activity.setStatus(2);
        assertEquals(2,activity.getStatus());
    }

    @Test
    void getAmountOfUsers(){
        Activity activity = new Activity();
        activity.setAmountOfUsers(2);
        assertEquals(2,activity.getAmountOfUsers());
    }

    @Test
    void activityToString(){
        Activity activity = new Activity();
        activity.setName("test");
        Category category = new Category("test");
        activity.setCategory(category);
        activity.setLocale(1);
        StringBuilder sb = new StringBuilder();
        sb.append("Activity[ name = ")
                .append(activity.getName())
                .append("; categories = ")
                .append(activity.getCategory()).append(activity.getLocale());
       assertEquals("Activity[ name = test; categories = Category [name=test]1", sb.toString());

    }
}
